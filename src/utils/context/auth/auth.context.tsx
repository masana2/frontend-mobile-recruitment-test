import React, {createContext, FC, useContext, useState} from 'react';
import {axiosInstance} from '../../axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {IDENTITY_STORAGE_KEY} from '../../constants';
import {AuthContextState, Identity} from './auth.types';

// FIXME there are multiple issues in this file, right now the user can't be logged in properly

const useAuthProvider = () => {
  // when the user is logged in, they should not be able to access the login page
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  // app should not be usable when `isLoading` is true
  const [isLoading, setLoading] = useState(false);
  // logged-in user email, cleared when user is logged out
  const [identity, setIdentity] = useState<Identity | null>(null);

  // get the identity from the internal storage
  const getStorageIdentity = async (): Promise<Identity | null> => {
    const rawIdentity = await AsyncStorage.getItem(IDENTITY_STORAGE_KEY);
    if (rawIdentity) {
      const parsedIdentity = JSON.parse(rawIdentity) || {};

      if (!identity) {
        setIdentity(parsedIdentity);
        setIsLoggedIn(true);
      }

      return parsedIdentity;
    }

    return null;
  };

  // set the identity in the internal storage
  const setStorageIdentity = async (newIdentity: Identity) => {
    await AsyncStorage.setItem(
      IDENTITY_STORAGE_KEY,
      JSON.stringify(newIdentity),
    );
    setIdentity(newIdentity);
  };

  // clear the identity storage key
  const removeStorageIdentity = () =>
    AsyncStorage.removeItem(IDENTITY_STORAGE_KEY);

  // login util function
  const login = async (body: {email: string; password: string}) => {
    setLoading(true);

    const response = await axiosInstance
      .post('/api/login', body)
      .catch(error => {
        return error;
      });

    if (response.request.status === 200) {
      setIsLoggedIn(true);
      await setStorageIdentity({email: body.email});
    }

    return response;
  };

  // logout util function
  const logout = async () => {
    setLoading(true);

    const response = await axiosInstance.post('/api/logout').catch(error => {
      return error;
    });

    if (response.status === 200) {
      setIdentity(null);
      await removeStorageIdentity();
      setIsLoggedIn(false);
    }

    return response;
  };

  return {isLoggedIn, isLoading, login, logout, identity};
};

const AuthContext = createContext<AuthContextState>({} as AuthContextState);

export const AuthProvider: FC = ({children}) => {
  const auth = useAuthProvider();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
};

export const useAuthState = () => {
  return useContext(AuthContext);
};
