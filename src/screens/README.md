# SCREENS

## Home

Will contain the home page, [read here](../../README.md#home-screen) for more info and requirements

## Login

Will contain the login page, [read here](../../README.md#login-screen) for more info and requirements

## Note-details

Will contain the note details page, [read here](../../README.md#note-details-screen) for more info and requirements
