# UTILS

## Context

This implements the authentication logic (login/logout functions, states, ect...)

## Axios

This mainly declares the common axios instance used in `api`

> You don't need to modify anything inside this folder

## Constants

Any constant for magic numbers or static string goes here.

> You don't need to modify anything inside this folder
